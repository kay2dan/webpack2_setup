/* eslint-disable */
const path = require("path");
const merge = require("webpack-merge");
// const validator = require( "webpack2-validator" );
const parts = require( "./webpack.config.parts" );
const PATHS = {
   app : path.join( __dirname, "app" ),
   build : path.join( __dirname, "build" )
};
const build_env = process.env.BUILD_ENV;
console.log( "build_env is... >>>>>>>>>>>>>>>>>>>>>", build_env, "<<<<<<<<<<<<<<<" );

const common = {
   entry : {
      // we will extend this with additional files to build
      app : PATHS.app + "/index.js"
   },
   output : {
      filename : "run.build.js",
      path : PATHS.build
   },
   resolve : {
      // aliases help us with navigating the files, when we imports in es2015
      // instead of something like `../../assets/aPng.png`,
      // we can do `assets/aPng.png`.
      // add / amend as many aliases below as needed
      alias : {
         assets : path.resolve( __dirname, "app/assets" ),
         components : path.resolve( __dirname, "app/components" )
      },
      // webpack2 doesn't require the 'empty' string in extensions anymore
      extensions : [ "js", "jsx" ]
   },
   resolveLoader : {
     modules : [ "./node_modules", PATHS.app ]
   }
};

var config;

switch( process.env.npm_lifecycle_event ){
  case( "build-Prod" ): {
    config = merge( common,
                      parts.defineENV(),
                      parts.sourceMaps( build_env ),
                      parts.babel( build_env, PATHS.app ),
                      parts.cssloader( PATHS.app ),
                      parts.fileloader( PATHS.app ));
    break;
	}
  case( "start-Dev" ):
  default: {
		const eslintPath = path.join( __dirname, "/.eslintrc" );
		config = merge( common,
										parts.defineENV(),
										// parts.sourceMaps( build_env ),
										// parts.eslint( PATHS.app, eslintPath ),
										parts.babel( build_env, PATHS.app ),
										// parts.cssloader( PATHS.app ),
										// parts.urlloader( PATHS.app ),
										// parts.fileloader( PATHS.app ),
										parts.devServer( PATHS.app ),
										parts.htmlWebpackPlugin( build_env ));
	}
}

console.log( "config.module is...", config.module );
// it seems `webpack2-validator` is currently not compatible with the new webpack2
// obj sequence!!! -_- Until then, its being commented.
module.exports = config;
