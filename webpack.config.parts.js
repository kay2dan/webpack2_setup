/* eslint-disable */
const webpack = require("webpack");
const HtmlWebpackPlugin = require("html-webpack-plugin");

/**
 * Webpack2 for 'production' needs to have the 'es2015' modules enabled in order
 * for tree-shaking (dead code removal) to work. However, in 'development', we
 * have to have babel conversion to commonJS enabled because browsers do not
 * support es2015 modules out of the box as yet.
 * @param  {str} buildType dev or prod for modules conversion
 * @param  {str} path      file path reference
 * @return {obj}
 */
exports.babel = function( buildType, path ){
	var standardPresets = [
              "es2015",
							"react"
  ];
	var presetsToLoad;
	if( buildType === "__PROD__" ){
		presetsToLoad = standardPresets.push({ modules : false });
	} else {
		presetsToLoad = standardPresets;
	}
	console.log( "presets are...", presetsToLoad );
	return({
		module: {
			rules : [
				{
					test : /\.jsx?$/,
          include : path,
					use : [
						{
							loader: "babel-loader",
							options : {
								presets : presetsToLoad
							}
						}
					]
				}
			]
		}
	});
};


/**
 * cssloader is for loading css styles; includes support for stylus
 * @param  {str} path directory path
 * @return {obj}
 */
exports.cssloader = function( path ){
  return({
    module : {
      rules : [
        {
          test : /\.(css|styl)$/,
          include : path,
          use : [
            {
              loader : "style-loader"
            },
            {
              loader : "css-loader"
            },
            {
              loader : "stylus-loader"
            }
          ]
        }
      ]
    }
  });
};


/**
 * Define the node environment for the webpack to run;
 * We can call the webpack with one of these variables & the build
 * will ignore code from other environment.
 * @return {obj} 'plugin' for importing into webpack plugins
 */
exports.defineENV = function() {
  return ({
    plugins: [
      new webpack.DefinePlugin({
        __DEV__: JSON.stringify(JSON.parse(process.env.BUILD_DEV || true)),
        __PROD__: JSON.stringify(JSON.parse(process.env.BUILD_PROD || false))
      })
    ]
  });
};


/**
 * The devServer that comes with webpack
 * @return {obj}         devServer config.
 */
exports.devServer = function() {
  return ({
    // these options are for windows & ubuntu specific machines
    // which may have problem with immediate reload so we set delay.
    // enable if auto server update doesn't properly function
    // watchOptions : {
    // 	aggregateTimeout : 300,
    // 	poll : 1000
    // },
    devServer: {
      historyApiFallback: true,
      hot: true,
      inline: true,
      stats: "errors-only"
    },
    plugins: [
      new webpack.HotModuleReplacementPlugin({
        multiStep: true
      })
    ]
  });
};




/**
 * eslint
 * @param  {str} path           folder to search in
 * @param  {str} configFilePath where the `.eslintrc` file is stored
 * @return {obj}
 */
exports.eslint = function(path, configFilePath ){
  return ({
    module: {
      rules : [
			{
				test : /\.jsx?$/,
				enforce : "pre",
        include : path,
				use : [
					{
						loader : "eslint-loader",
						options : {
							configFile : configFilePath
						}
					}
				]
			}
		]
	}
  });
};


/**
 * fileloader for loading up assets as per the regex in the test case
 * @param  {str} path path to the app folder which houses the assets
 * @return {obj}
 */
exports.fileloader = function( path ){
	return({
		module : {
			rules : [
				{
					test : /\.(jpg|png|svg)$/,
					include : path,
					use : [
						{
							loader : "file"
						}
					]
				}
			]
		}
	});
};


/**
 * This module inserts a plugin for creating a html file for displaying
 * in the browser. Being used in the 'app' repo purely for testing purposes.
 * @return { obj } containing the plugin
 */
exports.htmlWebpackPlugin = function() {
  return ({
    plugins: [
      new HtmlWebpackPlugin({
        title: "For Testing only!"
      })
    ]
  });
};


/**
 * The minification of the build for the production code.
 * @return {obj} plugin extension
 */
exports.minify = function(){
  return ({
    plugins: [
      new webpack.optimize.UglifyJsPlugin({
        beautify: false,
        comments: false, // remove comments
        compress: {
          warnings: false
        },
        mangle: {
          except: ["$"],
          screw_ie8: true, // yes please!
          keep_fnames: false // dont keep function names
        }
      })
    ]
  });
};


/**
 * The type of sourcemaps we want to use for different build
 * systems within the app. Can be extended for different scenarios
 * by the addition of additional 'if' conditions.
 * For now, 'buildType' is only checking for production, however,
 * we can / will be extending it based on performance issues associated
 * with sourcemaps, for different build types. For sourcemap types, see:
 * https://webpack.js.org/configuration/devtool/
 * @param  {string} buildType node env variable declaring the env running
 * @return {obj}           	obj for merging into the config
 */
exports.sourceMaps = function(buildType) {
  if (buildType === "__PROD__") {
    return ({
      devtool: "source-map"
    });
  } else {
    return ({
      devtool: "cheap-module-eval-source-map"
    });
  }
};


/**
 * The urlloader is for inlining (conversion to base64 format) assets like pngs
 * Note, we should always shrink the images before using services like...
 * https://tinypng.com/ etc
 * @param  {str} path folder path to scan
 * @return {obj}
 */
exports.urlloader = function( path ){
  return({
    module : {
      rules : [
        {
          test : /\.(jpg|png|svg)$/,
          include : path,
          use : [
            {
              loader : "urlloader",
              options : {
                limit : 25000
              }
            }
          ]
        }
      ]
    }
  });
};
